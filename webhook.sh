#!/bin/bash

if [[ -v CUSTOM_CONTENT ]]; then
    echo "use content from param"
    PAYLOAD=$CUSTOM_CONTENT
else
    PAYLOAD="{
        \"username\": \"Mabar Yuk\",
        \"avatar_url\": \"https://i.imgur.com/Zztry54g.jpg\",
        \"embeds\": [{
            \"description\": \"Maen gak guys?\"
        }]
    }"
fi

curl -X POST -H "Content-Type: application/json" -d "$PAYLOAD" "$WEBHOOK_URL"