# discord-webhook-trigger
To trigger the pipeline or create new pipeline schedule, you need to set these variable:
- `WEBHOOK_URL` : it's your discrod server webhook URL
- `CUSTOM_CONTENT`(optional): use JSON format to construct the payload. Use this [Page](https://birdie0.github.io/discord-webhooks-guide/index.html) as Guide
    - Default content if the `CUSTOM_CONTENT` not set:
        ```
        {
            "username": "Mabar Yuk",
            "avatar_url": "https://i.imgur.com/Zztry54g.jpg",
            "embeds": [{
                "description": "Maen gak guys?"
            }]
        }
        ```

Default Pipeline Schedule:
- Triggered every Saturday & Sunday at 19:00 GMT+7